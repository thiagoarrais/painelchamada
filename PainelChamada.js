import React, { useState } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  Row,
} from 'reactstrap';

import { useAuth0 } from 'Components/react-auth0-spa';

const PainelChamada = (props) =>  {
  const { getTokenSilently } = useAuth0();
  
  const [data, setData] = useState({pedido: null});
  
  const doSubmit = () => {
    getTokenSilently().then(token => {
      return fetch(`/chamada/${data.pedido}`, {method: 'POST', headers: {'Authorization': `Bearer ${token}`}})
        .then(resp => resp.json());
      }
    );
  };

  return (
    <Card>
      <CardHeader>
        <strong>Pedido</strong>
      </CardHeader>
      <CardBody>
        <Row>
          <Col xs="12">
            <Form onSubmit={(e) => {e.preventDefault(); doSubmit();}}>
              <InputGroup>
                <Input
                    type="text"
                    id="name"
                    placeholder="999"
                    onChange={e => {const value = e.target.value; setData(data => ({...data, pedido: value}));}}
                    value={data.pedido}
                    required
                />
                <InputGroupAddon addonType="append">
                  <Button
                    type="submit"
                    color="primary"
                  >
                    <i class="icon-paper-plane" title="Chamar"></i>
                  </Button>
                </InputGroupAddon>
              </InputGroup>
            </Form>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
}

export default PainelChamada;